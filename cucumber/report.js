$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("bdd/PurchaseConfig.feature");
formatter.feature({
  "id": "purchase-configuration",
  "description": "\r\nThis feature will allow users to configure purchase invoice. This feature will primarily be used for issuing Purchase \r\nInvoice.\r\n\r\nFunctionalities:\r\n1. Create new Configuration\r\n2. Get and Update existing configuration\r\n3. Delete and existing configuration.\r\n\r\nRules:\r\n1. Sum of percentages of Purchase Config items should be 100\r\n2. Each of the item should not have type as blank",
  "name": "Purchase Configuration",
  "keyword": "Feature",
  "line": 1
});
formatter.before({
  "duration": 2745495710,
  "status": "passed"
});
formatter.background({
  "description": "",
  "name": "",
  "keyword": "Background",
  "line": 15,
  "type": "background"
});
formatter.step({
  "name": "I am a valid user, have logged in and navigated to Purchase Configuration page",
  "keyword": "Given ",
  "line": 16
});
formatter.match({
  "location": "PurchaseConfig.loadRMSnClickPOConfig()"
});
formatter.write("Lanching RMS");
formatter.write("Next button for authorizing through mobile device did not appear. Probably this was skipped..");
formatter.write("Waiting for page where google asks permission to access account");
formatter.write("Got intermediate page to remember selection and choose account....clicked");
formatter.write("Clicked Purchase\u003e\u003ePO Config Button");
formatter.result({
  "duration": 69462971417,
  "status": "passed"
});
formatter.scenario({
  "id": "purchase-configuration;the-system-should-allow-me-to-save-a-new-po-config",
  "description": "",
  "name": "The system should allow me to save a new PO config",
  "keyword": "Scenario",
  "line": 19,
  "type": "scenario",
  "comments": [
    {
      "value": "# brand, VAT, type, sub type is set",
      "line": 17
    }
  ]
});
formatter.step({
  "name": "I click New PO config details button",
  "keyword": "When ",
  "line": 21,
  "comments": [
    {
      "value": "#  Given I am a valid user, have logged in and navigated to Purchase Configuration page",
      "line": 20
    }
  ]
});
formatter.step({
  "name": "enter PO Config details",
  "keyword": "And ",
  "line": 22,
  "rows": [
    {
      "cells": [
        "BHARAT",
        "3",
        "15200",
        "18",
        "0",
        "Gangaj Pumpset"
      ],
      "line": 23
    }
  ]
});
formatter.step({
  "name": "Enter PO Item details as",
  "keyword": "And ",
  "line": 24,
  "rows": [
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "",
        "60"
      ],
      "line": 25
    },
    {
      "cells": [
        "Submersible",
        "1.5 HP",
        "10 Stage",
        "30"
      ],
      "line": 26
    },
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "12 Stage",
        "10"
      ],
      "line": 27
    }
  ]
});
formatter.step({
  "name": "click save button",
  "keyword": "And ",
  "line": 28
});
formatter.step({
  "name": "when most recently created PO is clicked",
  "keyword": "Then ",
  "line": 29
});
formatter.step({
  "name": "the saved order should have PO details as",
  "keyword": "And ",
  "line": 30,
  "rows": [
    {
      "cells": [
        "BHARAT",
        "3",
        "15200",
        "18",
        "0",
        "Gangaj Pumpset"
      ],
      "line": 31
    }
  ]
});
formatter.step({
  "name": "saved order 1 should have PO item details as",
  "keyword": "And ",
  "line": 32,
  "rows": [
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "",
        "60"
      ],
      "line": 33
    },
    {
      "cells": [
        "Submersible",
        "1.5 HP",
        "10 Stage",
        "30"
      ],
      "line": 34
    },
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "12 Stage",
        "10"
      ],
      "line": 35
    }
  ]
});
formatter.step({
  "name": "System should display the PO number",
  "keyword": "And ",
  "line": 36
});
formatter.step({
  "name": "System should display success message",
  "keyword": "And ",
  "line": 37
});
formatter.match({
  "location": "PurchaseConfig.click_new_po_config_button()"
});
formatter.write("Clicked new PO button");
formatter.result({
  "duration": 5689810577,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.click_new_po_config_button_enter_with_details(DataTable)"
});
formatter.result({
  "duration": 4129525949,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.enter_po_item_details(DataTable)"
});
formatter.write("Clicked ADD items button 3 times");
formatter.write("Added  Config items 1");
formatter.write("Added  Config items 2");
formatter.write("Added  Config items 3");
formatter.result({
  "duration": 5409580018,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.click_save_button()"
});
formatter.write("Clicked SAVE button");
formatter.result({
  "duration": 114243761,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.when_most_recent_PO_is_clicked()"
});
formatter.write("Click SIde Link...");
formatter.write("Click PO Detaisl from list...");
formatter.result({
  "duration": 9371140859,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.when_PO_is_clicked_the_saved_order_should_have_PO_details_as(DataTable)"
});
formatter.result({
  "duration": 5069092077,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 12
    }
  ],
  "location": "PurchaseConfig.saved_order_should_have_PO_item_details_as(int,DataTable)"
});
formatter.write("Added  Config items 1");
formatter.write("Added  Config items 2");
formatter.write("Added  Config items 3");
formatter.result({
  "duration": 177657010,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "duration": 14530,
  "status": "passed"
});
formatter.before({
  "duration": 154260,
  "status": "passed"
});
formatter.background({
  "description": "",
  "name": "",
  "keyword": "Background",
  "line": 15,
  "type": "background"
});
formatter.step({
  "name": "I am a valid user, have logged in and navigated to Purchase Configuration page",
  "keyword": "Given ",
  "line": 16
});
formatter.match({
  "location": "PurchaseConfig.loadRMSnClickPOConfig()"
});
formatter.write("Clicked Purchase\u003e\u003ePO Config Button");
formatter.result({
  "duration": 267132737,
  "status": "passed"
});
formatter.scenario({
  "id": "purchase-configuration;the-system-should-allow-me-to-edit-and-save-existing-po-config",
  "description": "",
  "name": "The system should allow me to edit and save existing PO config",
  "keyword": "Scenario",
  "line": 39,
  "type": "scenario"
});
formatter.step({
  "name": "I click New PO config details button",
  "keyword": "Given ",
  "line": 40
});
formatter.step({
  "name": "enter PO Config details",
  "keyword": "And ",
  "line": 41,
  "rows": [
    {
      "cells": [
        "BHARAT",
        "3",
        "15200",
        "18",
        "0",
        "Gangaj Pumpset"
      ],
      "line": 42
    }
  ]
});
formatter.step({
  "name": "Enter PO Item details as",
  "keyword": "And ",
  "line": 43,
  "rows": [
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "",
        "60"
      ],
      "line": 44
    },
    {
      "cells": [
        "Submersible",
        "1.5 HP",
        "10 Stage",
        "30"
      ],
      "line": 45
    },
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "12 Stage",
        "10"
      ],
      "line": 46
    }
  ]
});
formatter.step({
  "name": "click save button",
  "keyword": "And ",
  "line": 47
});
formatter.step({
  "name": "when most recently created PO is clicked",
  "keyword": "When ",
  "line": 48
});
formatter.step({
  "name": "click on Edit button",
  "keyword": "And ",
  "line": 49
});
formatter.step({
  "name": "enter PO Config details",
  "keyword": "And ",
  "line": 50,
  "rows": [
    {
      "cells": [
        "BHARAT",
        "3",
        "15200",
        "18",
        "0",
        "Gangaj Pumpset - edited"
      ],
      "line": 51
    }
  ]
});
formatter.step({
  "name": "delete existing PO Items",
  "keyword": "And ",
  "line": 52
});
formatter.step({
  "name": "Enter PO Item details as",
  "keyword": "And ",
  "line": 53,
  "rows": [
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "",
        "60"
      ],
      "line": 54
    },
    {
      "cells": [
        "Submersible",
        "1.5 HP",
        "10 Stage",
        "30"
      ],
      "line": 55
    },
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "12 Stage",
        "10"
      ],
      "line": 56
    }
  ]
});
formatter.step({
  "name": "click save button",
  "keyword": "And ",
  "line": 57
});
formatter.step({
  "name": "when most recently created PO is clicked",
  "keyword": "Then ",
  "line": 58
});
formatter.step({
  "name": "the saved order should have PO details as",
  "keyword": "And ",
  "line": 59,
  "rows": [
    {
      "cells": [
        "BHARAT",
        "3",
        "15200",
        "18",
        "0",
        "Gangaj Pumpset - edited"
      ],
      "line": 60
    }
  ]
});
formatter.step({
  "name": "saved order 1 should have PO item details as",
  "keyword": "And ",
  "line": 61,
  "rows": [
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "",
        "60"
      ],
      "line": 62
    },
    {
      "cells": [
        "Submersible",
        "1.5 HP",
        "10 Stage",
        "30"
      ],
      "line": 63
    },
    {
      "cells": [
        "Control Panel",
        "1.5 HP",
        "12 Stage",
        "10"
      ],
      "line": 64
    }
  ]
});
formatter.step({
  "name": "System should display the PO number",
  "keyword": "And ",
  "line": 65
});
formatter.step({
  "name": "System should display success message",
  "keyword": "And ",
  "line": 66
});
formatter.match({
  "location": "PurchaseConfig.click_new_po_config_button()"
});
formatter.write("Clicked new PO button");
formatter.result({
  "duration": 5619469612,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.click_new_po_config_button_enter_with_details(DataTable)"
});
formatter.result({
  "duration": 3975906021,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.enter_po_item_details(DataTable)"
});
formatter.write("Clicked ADD items button 3 times");
formatter.write("Added  Config items 1");
formatter.write("Added  Config items 2");
formatter.write("Added  Config items 3");
formatter.result({
  "duration": 4591630314,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.click_save_button()"
});
formatter.write("Clicked SAVE button");
formatter.result({
  "duration": 262792930,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.when_most_recent_PO_is_clicked()"
});
formatter.write("Click SIde Link...");
formatter.write("Click PO Detaisl from list...");
formatter.result({
  "duration": 9320528222,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.click_on_Edit_button()"
});
formatter.result({
  "duration": 111326587,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.click_new_po_config_button_enter_with_details(DataTable)"
});
formatter.result({
  "duration": 3973442562,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.delete_existing_PO_items()"
});
formatter.result({
  "duration": 3214532012,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.enter_po_item_details(DataTable)"
});
formatter.write("Clicked ADD items button 3 times");
formatter.write("Added  Config items 1");
formatter.write("Added  Config items 2");
formatter.write("Added  Config items 3");
formatter.result({
  "duration": 4651418967,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.click_save_button()"
});
formatter.write("Clicked SAVE button");
formatter.result({
  "duration": 91145875,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.when_most_recent_PO_is_clicked()"
});
formatter.write("Click SIde Link...");
formatter.write("Click PO Detaisl from list...");
formatter.result({
  "duration": 9344692977,
  "status": "passed"
});
formatter.match({
  "location": "PurchaseConfig.when_PO_is_clicked_the_saved_order_should_have_PO_details_as(DataTable)"
});
formatter.result({
  "duration": 5085862726,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 12
    }
  ],
  "location": "PurchaseConfig.saved_order_should_have_PO_item_details_as(int,DataTable)"
});
formatter.write("Added  Config items 1");
formatter.write("Added  Config items 2");
formatter.write("Added  Config items 3");
formatter.result({
  "duration": 165915572,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "duration": 10691,
  "status": "passed"
});
});